package cz.vse.grytsak;

/**
 * Třída slouží k zapnutí štítu
 * 
 * @author Leon Grytsak
 * @version 1.0
 */
public class CommandShield implements ICommand
{
    private static final String NAME = "stit"; 
    private GamePlan plan;

    /**
     * Konstruktor třídy
     * 
     * @param plan přijímá aktuální stav hry z GamePlan a ukládá jej do proměnné plan
     */
    public CommandShield(GamePlan plan)
    {
        this.plan = plan;    
    }    

    /**
     * Metoda slouží k nastavení silového pole na true (zapnuto)
     * Pokud hráč pole již ve hře použil, nemůže ho aktivovat znova
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length > 0) {
            return "K tomuto příkazu nezadávejte parametry.";
        }

        if(!plan.getAtributes().getShieldUsed()) {        
            plan.getAtributes().setShield(true);
            return "Orsin: Silové pole je zapnuté a ochraní tě před dalším útokem příšery.";
        }
        return "Orsin: Štít jsem už použil!"; 
    }

    /**
     * Metoda vrací název příkazu
     * 
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }
}