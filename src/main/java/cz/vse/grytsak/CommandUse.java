package cz.vse.grytsak;
import java.util.*;

/**
 * Třída představuje příkaz pouzij. Třída rozhoduje o tom co se stane pokud použijete příkaz použíj s daným parametrem (nějaký použitelný předmět)
 * 
 * @author Leon Grytsak
 * @version 1.0
 */
public class CommandUse implements ICommand
{
    private static final String NAME = "pouzij"; 
    GamePlan plan = new GamePlan(); //nová instance GamePlan
    private double modifierHealth = 0;
    private double modifierSpeed = 0;
    private int min = 0;
    private int max = 0;

    /**
     * Konstruktor třídy
     * 
     * @param plan příjme přeposlaný současný průběh hry 
     */
    public CommandUse(GamePlan plan)
    {
        this.plan = plan;    
    }

    /**
     * Metoda určuje co se stane pokud uživatel použije předmět. Pokud je parametr zadán špatne metoda vyhodí výstup o tom že neví jaký předmět má použít.
     * Pokud bylo zadáno více parametru metoda vyhodí výstup o tom že neumí použít více předmětů naráz
     * Pokud byl předmět vybrán správně, metoda vyhodnotí zda se předmět vůbec nachází v invetáři hráče
     * 
     * @param parameters název předmětu který se má použít
     * @return dialog podle výsledku (špatný parametr, více parametrů, předmět není v invetáři anebo nejde použít, předmět byl použit)
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length == 0) {
            return "Orsin: Nevím co mám použít, musíš zadat název předmětu.";
        }

        if (parameters.length > 1) {
            return "Orsin: Tomu nerozumím, neumím použít více předmětů najednou.";
        }

        String itemName = parameters[0];
        if(!plan.getInventory().containsItem(itemName)) {
            return "Předmět " + itemName + " nemáš v inventáři a nebo nejde použít.";  
        } 

        if(plan.getInventory().containsItem(itemName)) {
            switch (itemName) {         
                case "obvazy": 
                min = 15;
                max = 30;
                modifierHealth = rngNum(min,max);
                plan.getAtributes().setHealth(modifierHealth);
                plan.getInventory().deleteItem(itemName);
                return "Orsin: použil jsem obvazy. Zdráví se změnilo o " + modifierHealth + " jednotek a momentálně mám " + plan.getAtributes().getHealth() + " životů a " + plan.getAtributes().getSpeed() + " rychlosti.";
                case "lekarnicka":
                min = 20;
                max = 40;
                modifierHealth = rngNum(min,max);
                min = 10;
                max = 20;
                modifierSpeed = rngNum(min,max);
                plan.setItemsSpeed(modifierSpeed);
                plan.getAtributes().setHealth(modifierHealth);
                plan.getAtributes().setSpeedItems(modifierSpeed);
                plan.getAtributes().setSpeed();
                plan.getInventory().deleteItem(itemName);
                return "Orsin: použil jsem lekárničku. Rychlost se změnila o: " + modifierSpeed + "jednotek a zdraví o: " + modifierHealth + " jednotek \n a momentálně mám " + plan.getAtributes().getHealth() + " životů a " + plan.getAtributes().getSpeed() + " rychlosti.";
            }
        } 
        return "Předmět nejde použít, ale sníst.";
    }    
    
    /**
     * Metoda vrátí název příkazu
     * 
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    } 
    
    /**
     * Metoda slouží k randomování čísla
     * 
     * @param min, max určijí rozmezí pro randomování čísla
     * @return nahodně vylosované číslo 
     */
    public int rngNum(int min, int max)
    {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

}