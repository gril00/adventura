package cz.vse.grytsak;
import java.util.Random;

/**
 * Třída předsavuje příšeru která útočí na hráče během hraní
 * 
 * @author Leon Grytsak
 * @version 1.0
 */
public class Monster
{
    private double damage;
    private double stanceDamage;
    private double rngMove;
    private int min;
    private int max;
    private int chance;
    private boolean hiddenUnharmed;
    boolean monsterAttacked = false;
    private GamePlan plan;
    private static final String ATTACK_TEXT = "Herní postava umřela na útok příšery.";
    private static final String DAMAGE_TEXT = " jednotek poškození.";

    /**
     * Konstruktor třídy  nastavuje hodnoty proměnným a zároveň přijímá aktuální stav hry z GamePlan
     * 
     * @param plan přijímá aktualní stav hry z GamePlan do proměnné plan
     */
    public Monster(GamePlan plan)
    {
        min = 0; 
        max = 55;
        stanceDamage = 1.00;
        damage = 0.00;
        hiddenUnharmed = false;
        this.plan = plan;
    }

    /**
     * Metoda představuje útok na hráče. Metoda se volá vždy když hráč přejde z jedné lokace do jiné.
     * Před útokem se rozhoduje o mnoha věcech prostřednictvím randomu. Zprva jaká je šance na to že příšera zautočí. Ta se odvíjí od toho kolik má hráč častí klíčů
     * a nebo jestli má opravený klíč.
     * Před útokem se také spočítá poškození které hráč dostane pokud má postoj 'skryty' a zároveň random hodnota který udává zda ho postoj ochrání nebo ne.
     * Pokud příšera zautočí a postoj 'skryty' hráče ochrání, nedostane žádne poškození, ale pokud ho postoj 'skryty' neochrání dostane bonusové poškození podle toho
     * kolik mu příšera udělila poškození což se odvíjí na náhodě a rychlosti hráče.
     * Dále metoda reague na stav štítu hráče. Pokud je štít aktivovaný a nestvůra zautočí, hráč nedostane poškození a štít se deaktivuje a již ho není možné aktivovat.
     * Pokud měl hráč postoj 'skryty' a ten ho ochráním a štít byl aktivní, štít se nespoužije. Ale pokud ho postoj 'skryty' neochrání, ochrání ho štít a ten se spotřebuje
     * a již ho není možné aktivovat.
     * Poškození se odvíjí podle toho jakou má hráč rychlost a také podle náhody. Pokud má hráč malou rychlost, existuje velmi malá šance na to že mu příšera udělí 100 bodů poškození
     * a hráč prohraje hru.
     *
     * @return výsledek útoku nestvůry
     */
    public String monsterAttack()
    {
        plan.getAtributes().setHealth(0);
        chance = 15;
        stanceDamage = 1;
        damage = 0.00;
        monsterAttacked = false;
        switch (plan.getKeyInventory().keyParts()) {
            case 1:
            chance = 23;
            break;
            case 2:
            chance = 30;
            break;
            case 3:
            chance = 38;
            break;
            case 4:
            chance = 46;
            break;
        }        
        if(plan.getKeyInventory().repairedKey()) {
            chance = 46;
        }
        if(plan.getAtributes().getStance()) {
            rngMove = rngNumber(20, 45);
            rngMove = rngMove / 100;
            stanceDamage = stanceDamage + rngMove;
            rngMove = rngNumber (0,10);
            if(rngMove >= 0 & rngMove <= 4) {
                hiddenUnharmed = true;  
            }
            else {
                hiddenUnharmed = false;
            }
        }
        min = 0;
        max = 55;
        rngMove = rngNumber(min, max);
        if(rngMove >= 0 & rngMove <= chance) {
            monsterAttacked = true;
            plan.getAtributes().setHealth(0);
            double speed = plan.getAtributes().getSpeed();
            if(!hiddenUnharmed) {
                if(!plan.getAtributes().getShield()) {

                    if(speed > 89 & speed <= 100) {
                        return "Orsin: Napadla mě příšera, ale podařilo se mi utéct bez zranění.";
                    }
                    else if(speed > 79 & speed <= 89) { 
                        min = 10;
                        max = 15;
                        damage = rngNumber(min, max) * stanceDamage;
                        plan.getAtributes().setHealth(damage*-1);

                        if(plan.getAtributes().getHealth() == 0) {

                            return ATTACK_TEXT;
                        }

                        return "Orsin: Napadla mě příšera, málem mě dostala, ale podařilo se mi uprchnout s malými zraněními.\n Příšera udělila " + damage + DAMAGE_TEXT;
                    }
                    else if(speed > 59 & speed <= 79) {
                        chance = rngNumber(0, 55);
                        if(chance <= 40) {
                            min = 15;
                            max = 30;
                        }                        
                        if(chance >= 41 & chance <= 55) {
                            min = 20;
                            max = 40;
                        }
                        damage = rngNumber(min, max) * stanceDamage;
                        plan.getAtributes().setHealth(damage*-1);
                        if(plan.getAtributes().getHealth() == 0) {
                            return ATTACK_TEXT;
                        }
                        if(chance <= 40) {
                            return "Orsin: Napadla mě příšera. Utrpěl jsem průměrně velké škody na zdraví, ale podařilo se mi uprchnout.\n Příšera udělila " + damage + DAMAGE_TEXT;
                        }                        
                        if(chance >= 41 & chance <= 55) {
                            return "Orsin: Napadla mě příšera. Utrpěl jsem velké škody na zdraví, ale podařilo se mi uprchnout.\n Příšera udělila " + damage*100 + DAMAGE_TEXT;
                        }
                    }
                    else if(speed > 0 & speed <= 59) {
                        chance = rngNumber(0, 55);
                        if(chance <= 35){
                            min = 30;
                            max = 40;
                        }
                        if(chance >= 36 & chance < 55) {
                            min = 40;
                            max = 60;
                        }                        
                        if(chance == 55) {
                            min = 100;
                            max = 100;
                        }
                        damage = rngNumber(min, max) * stanceDamage;
                        plan.getAtributes().setHealth(damage*-1);
                        if(plan.getAtributes().getHealth() == 0) {
                            return ATTACK_TEXT;
                        }
                        if(chance <= 35) {
                            return "Orsin: Napadla mě příšera. Utrpěl jsem velmi velké škody na zdraví, ale podařilo se mi uprchnout.\n Příšera udělila " + damage + DAMAGE_TEXT;
                        }                        
                        if(chance >= 36 & chance <= 54) {
                            return "Orsin: Napadla mě příšera. Utrpěl jsem nehorázné škody na zdraví, ale podařilo se mi uprchnout.\n Příšera udělila " + damage + DAMAGE_TEXT;
                        }                        
                        if(chance == 55) {
                            return ATTACK_TEXT;
                        }
                    }
                }
                plan.getAtributes().setShield(false);
                return "Příšera zautočila, ale silové pole tě ochránilo.";
            }
            return "Příšera byla v lokaci, ale podařilo se ti před ní skrýt.";
        }
        return "Příšera nezautočila.";
    }

    /**
     * Metoda sloužící k náhodnému generování čísla
     * 
     * @param min, max udává rozsah pro generování čísla
     * @return náhodně vygenerované číslo
     */
    private int rngNumber(int min, int max)
    {
        Random rng = new Random();

        int rngNum = rng.nextInt((max - min) + 1) + min;

        return rngNum;
    }

    /**
     * Metoda vrací šanci (chance) - metoda pro testy
     * 
     * @return šance
     */
    public int getChance()
    {
        return chance;
    }

    /**
     * Metoda nastaví šanci (chance) - metoda pro testy
     * 
     * @param modifier udává šanci
     */
    public void setChance(int modifier)
    {
        chance = modifier;
    }

    /**
     * Metoda vrací poškození z postoje - metoda pro testy
     * 
     * @return poškození z postoje
     */
    public double getStanceDamage()
    {
        return stanceDamage;
    }

    /**
     * Metoda vrací celkové poškození - metoda pro testy
     * 
     * @return celkové poškození
     */
    public double getDamage()
    {
        return damage;
    }

    public boolean getMonsterAttacked() {return monsterAttacked;}
}