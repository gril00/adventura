package cz.vse.grytsak;

/**
 * Třída slouží k vypsání atributů postavy do konzole
 * 
 * @author Leon Grytsak
 * @version 1.0
 */
public class CommandAtributes implements ICommand
{
    private static final String NAME = "atributy";

    GamePlan plan = new GamePlan();
    /**
     * Konstruktor třídy
     * 
     * @param plan přijímá aktualní stav hry z GamePlan a ukládá ho do proměnné plan
     */
    public CommandAtributes(GamePlan plan)
    {
        this.plan = plan;
    }

    /**
     * Metoda slouží k vypsání atributů. Metoda nejdřívě aktualizuje atributy životy a rychlost a poté vypíše jejich hodnoty
     * 
     * @param parameters parametry přijímá ale nevyužívá je
     * @retun životy a rychlost
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length > 0) {
            return "K tomuto příkazu nezadávejte parametry.";
        }
        plan.getAtributes().setHealth(0);
        return "Životy: " + plan.getAtributes().getHealth() + "\nRychlost: " + plan.getAtributes().getSpeed(); 
    }

    /**
     * Metoda vrací název příkazu
     * 
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }  
}