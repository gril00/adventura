package cz.vse.grytsak;
import java.util.*;

/**
 * Třída představující aktuální stav hry. Veškeré informace o stavu hry
 * <i>(mapa lokací, inventář, vlastnosti hlavní postavy, informace o plnění
 * úkolů apod.)</i> by měly být uložené zde v podobě datových atributů.
 * <p>
 * Třída existuje především pro usnadnění potenciální implementace ukládání
 * a načítání hry. Pro uložení rozehrané hry do souboru by mělo stačit uložit
 * údaje z objektu této třídy <i>(např. pomocí serializace objektu)</i>. Pro
 * načtení uložené hry ze souboru by mělo stačit vytvořit objekt této třídy
 * a vhodným způsobem ho předat instanci třídy {@link Game}.
 *
 * @author Michael Kölling
 * @author Luboš Pavlíček
 * @author Jarmila Pavlíčková
 * @author Jan Říha
 * @author Leon Grytsak
 * @version 1.0
 *
 * @see <a href="https://java.vse.cz/4it101/AdvSoubory">Postup pro implementaci ukládání a načítání hry na předmětové wiki</a>
 * @see java.io.Serializable
 */
public class GamePlan
{
    private static final String FINAL_LOCATION_NAME = "stara_budova";
    private static final String KEY_PART_TEXT = "Část klíče kterou potřebuju k sestavení klíče na otevření budovy s portálem.";

    private double itemsSpeed;
    List<String> areaForItem = new ArrayList<String>();

    private Area currentArea;
    private Inventory inventory;
    private KeyInventory invK;
    private Atributes atributes;
    private Monster monster;

    private Item kovarna;

    private Item boruvka;
    private Item jedla_trava;
    private Item ziva_kvetina;
    private Item brouk;
    private Item vejce;

    private Item obvazy;
    private Item medkit;

    private Item keypart1;
    private Item keypart2;
    private Item keypart3;
    private Item keypart4;

    private Area vchodDoLesa;
    private Area baziny;
    private Area jeskyne;
    private Area jeskyneHlubiny;
    private Area reka;
    private Area staraKovarna;
    private Area srdceLesa;
    private Area staraBudova;

    /**
     * Konstruktor třídy. Pomocí metody {@link #prepareWorldMap() prepareWorldMap}
     * vytvoří jednotlivé lokace a propojí je pomocí východů.
     */
    public GamePlan()
    {
        prepareWorldMap();
        inventory = new Inventory();
        invK = new KeyInventory();
        atributes = new Atributes();
        monster = new Monster(this);
    }


    /**
     * Metoda vytváří jednotlivé lokace a propojuje je pomocí východů. Jako
     * výchozí aktuální lokaci následně nastaví domeček, ve kterém bydlí
     * Karkulka.
     */
    private void prepareWorldMap()
    {
        // Vytvoříme jednotlivé lokace
        vchodDoLesa = new Area("vchod_do_lesa","Vchod do aeterického lesa před pevností.");
        baziny = new Area("baziny", "Jsi v bažinách. V dáli vidíš vchod do jeskyně a nebo předchod k řece.");
        jeskyne = new Area("jeskyne","Žeby se jednalo o doupě příšery? A hele! Támhle pokračuje.");
        jeskyneHlubiny = new Area("jeskyne_hlubiny","Hlubiny jeskyně. Měl bych být opatrný.");
        reka = new Area("reka","Vede sem hodně cest.");
        staraKovarna = new Area("stara_kovarna","Stará kovárna. Tady bych možná mohl opravit klíč.");
        srdceLesa = new Area("srdce_lesa","Srdce aeterického lesa. Skener zaznamenal velké množství energie.");
        staraBudova = new Area(FINAL_LOCATION_NAME,"Stara budova. Tady je můj portál!");

        // Nastavíme průchody mezi lokacemi (sousední lokace)
        vchodDoLesa.addExit(baziny);
        baziny.addExit(vchodDoLesa);

        baziny.addExit(jeskyne);
        jeskyne.addExit(baziny);

        jeskyne.addExit(jeskyneHlubiny);
        jeskyneHlubiny.addExit(jeskyne);

        jeskyne.addExit(reka);
        reka.addExit(jeskyne);

        reka.addExit(srdceLesa);
        srdceLesa.addExit(reka);

        reka.addExit(staraKovarna);
        staraKovarna.addExit(reka);

        srdceLesa.addExit(staraBudova);
        staraBudova.addExit(srdceLesa);

        // Přidáme předměty do lokací

        kovarna = new Item("kovarna","Tady můžu opravit klíč!",true, true);

        boruvka = new Item("boruvky", "Mimozemská borůvka", true, false);

        jedla_trava = new Item("jedla_trava", "Podivná jedlá tráva.", true, false);

        ziva_kvetina = new Item("zive_kvetiny", "Hýbající se květina.", true, false);

        brouk = new Item("brouky", "Mimozemský brouk. Vypadá jedle.", true, false);

        vejce = new Item("vejce", "Mimozemské vejce. Řekl bych že je zkažené.", true, false);

        obvazy = new Item("obvazy", "Obvazy. Ty se určitě hodí!", true, false);

        medkit = new Item("lekarnicka", "Lekarnička. Použít v největší nouzi!", true,false);


        keypart1 = new Item("prvni_cast_klice", KEY_PART_TEXT, false, false);
        keypart2 = new Item("druha_cast_klice", KEY_PART_TEXT, false, false);
        keypart3 = new Item("treti_cast_klice", KEY_PART_TEXT, false, false);
        keypart4 = new Item("ctvrta_cast_klice", KEY_PART_TEXT, false, false);



        staraKovarna.addItem(kovarna);
        rngAddItemToArea(boruvka);
        rngAddItemToArea(jedla_trava);
        rngAddItemToArea(ziva_kvetina);
        rngAddItemToArea(brouk);
        rngAddItemToArea(vejce);
        rngAddItemToArea(obvazy);
        rngAddItemToArea(medkit);
        rngAddItemToArea(keypart1);
        rngAddItemToArea(keypart2);
        rngAddItemToArea(keypart3);
        rngAddItemToArea(keypart4);

        // Hru začneme v první lokaci
        currentArea = vchodDoLesa;
    }

    /**
     * Metoda slouží pro náhodne generování čísla
     * 
     * @param maxAreas představuje maximální počet oblastí a také maximální možné vygenerované číslo
     * @return náhodně vygenerované číslo
     */
    private int rngNumber(int maxAreas)
    {
        Random rng = new Random();

        int rngNum = rng.nextInt(maxAreas);

        return rngNum;
    }

    /**
     * Metoda slouží k přidání itemů do lokace a zároveň omezuje max. počet itemů na 4 pro jednu lokaci
     * 
     * @param item předmět který se má přidat do lokace (není String ale Item)
     */
    public void rngAddItemToArea(Item item)
    {
        int maxAreas = 6; //počet místností ve kterých se můžou spawnout itemy
        int rngMove = 0;
        Collection<Item> itemList;

        rngMove = rngNumber(maxAreas);
        
        if(rngMove == 0) {
            itemList = baziny.getItemList().values();
            if(itemList.size() > 4) {
                rngAddItemToArea(item);
                itemList.clear();
            } else {
            baziny.addItem(item);}
        }
        if(rngMove == 1) {
            itemList = jeskyne.getItemList().values();
            if(itemList.size() > 4) {
                rngAddItemToArea(item);
                itemList.clear();
            } else {
            jeskyne.addItem(item);}
        }
        if(rngMove == 2) {
            itemList = jeskyneHlubiny.getItemList().values();
            if(itemList.size() > 4) {
                rngAddItemToArea(item);
                itemList.clear();
            } else {
            jeskyneHlubiny.addItem(item);}
        }
        if(rngMove == 3) {
            itemList = reka.getItemList().values();
            if(itemList.size() > 4) {
                rngAddItemToArea(item);
                itemList.clear();
            } else {
            reka.addItem(item);}
        }
        if(rngMove == 4) {
            itemList = staraKovarna.getItemList().values();
            if(itemList.size() > 4) {
                rngAddItemToArea(item);
                itemList.clear();
            } else {
            staraKovarna.addItem(item);}
        }
        if(rngMove == 5) {
            itemList = srdceLesa.getItemList().values();
            if(itemList.size() > 4) {
                rngAddItemToArea(item);
                itemList.clear();
            } else {
            srdceLesa.addItem(item);}
        }
        if(rngMove == 6) {
            itemList = staraBudova.getItemList().values();
            if(itemList.size() > 4) {
                rngAddItemToArea(item);
                itemList.clear();
            } else {
            staraBudova.addItem(item);}
        }
    }

    /**
     * Metoda vrací odkaz na aktuální lokaci, ve které se hráč právě nachází.
     *
     * @return aktuální lokace
     */
    public Area getCurrentArea()
    {
        return currentArea;
    }

    /**
     * Metoda nastaví aktuální lokaci, používá ji příkaz {@link CommandMove}
     * při přechodu mezi lokacemi.
     *
     * @param area lokace, která bude nastavena jako aktuální
     */
    public void setCurrentArea(Area area)
    {
        currentArea = area;
    }

    /**
     * Metoda slouží k vrácení lokace v Area podle jména String
     *
     * @param areaName nazev lokace v String
     * @return lokaci v datovém typu Area
     */
    public Area getCurrentAreaByName(String areaName){
        if(vchodDoLesa.getName().equals(areaName)){
            return vchodDoLesa;
        } else if (baziny.getName().equals(areaName)){
            return baziny;
        } else if (jeskyne.getName().equals(areaName)){
            return jeskyne;
        } else if (jeskyneHlubiny.getName().equals(areaName)){
            return jeskyneHlubiny;
        } else if (reka.getName().equals(areaName)){
            return reka;
        } else if (staraKovarna.getName().equals(areaName)){
            return staraKovarna;
        }else if (srdceLesa.getName().equals(areaName)){
            return srdceLesa;
        }else if (staraBudova.getName().equals(areaName)){
            return staraBudova;
        }
        return null;
    }

    /**
     *Metoda slouží k vrácení předmětu v Item podle jména v String
     *
     * @param itemName nazev předmětu v String
     * @return předmět v dat. typu Item
     */
    public Item getItemByName(String itemName) {
        if(boruvka.getName().equals(itemName)){
            return boruvka;
        } else if(jedla_trava.getName().equals(itemName)){
            return jedla_trava;
        } else if(ziva_kvetina.getName().equals(itemName)){
            return ziva_kvetina;
        } else if(brouk.getName().equals(itemName)){
            return brouk;
        } else if(vejce.getName().equals(itemName)){
            return vejce;
        } else if(obvazy.getName().equals(itemName)){
            return obvazy;
        } else if(medkit.getName().equals(itemName)){
            return medkit;
        } else if(keypart1.getName().equals(itemName)){
            return keypart1;
        } else if(keypart2.getName().equals(itemName)){
            return keypart2;
        } else if(keypart3.getName().equals(itemName)){
            return keypart3;
        } else if(keypart4.getName().equals(itemName)){
            return keypart4;
        }
        return null;
    }
    /**
     *Metoda slouží k ukončení hry pomocí výhry. Je volána z class Game.
     * Pokud se podmínky potvrdí vratí True což znamená že hráč vyhrál, jinak vrátí false což znamená že hra ještě probíhá a nebo
     * hráč prohrál.
     *
     * @return boolean výhra
     */
    public boolean isVictorious()
    {
        atributes.setHealth(0);        
        if (FINAL_LOCATION_NAME.equals(currentArea.getName()) && invK.repairedKey() && atributes.getHealth() > 0)
        {
            return FINAL_LOCATION_NAME.equals(currentArea.getName());
        }        
        return false;
    }

    /**
     *Metoda slouží k ukončení hry pomocí prohry. Je volána z Class Game.
     * Pokud se podmínky potvrdí a vrátí true což znamená že hráč prohrál, jinak vratí false což znamená že hra ještě probíhá a nebo
     * hráč vyhrál.
     *
     * @return boolean prohra
     */
    public boolean isDefeated()
    {
        if(getAtributes().getHealth() == 0){        
            return true;  
        }
        return false;
    }

    /**
     *Metoda slouží k zjískání inventáře z momentálního průběhu hry
     *
     * @return inventory vratí intentář
     */
    public Inventory getInventory()
    {
        return this.inventory;
    }

    /**
     *Metoda slouží k zjískání inventáře klíčů z momenátlního průběhu hry
     *
     * @return invK vratí inventář klíčů
     */
    public KeyInventory getKeyInventory()
    {
        return this.invK;
    }

    /**
     *Metoda slouží k zjískání třídy atributes z momenátlního průběhu hry
     *
     * @return atributes vrátí třídu Atributes
     */
    public Atributes getAtributes()
    {
        return this.atributes;
    }

    /**
     *Metoda slouží k zjískání lokace 'stará kovárna' z momenátlního průběhu hry
     *
     * @return startaKovarna v datovém typu Area
     */
    public Area getSmeltery()
    {
        return staraKovarna;
    }

    /**
     *Metoda slouží k zjískání třídy Monster z momenátlního průběhu hry
     *
     * @return monster
     */
    public Monster getMonster()
    {
        return monster;
    }

    /**
     *Metoda slouží k zjískání modifikatoru speed items z třídy Atributes
     *
     * @param speed modifikator poslaný z CommandEat a CommandUse
     */
    public void setItemsSpeed(Double speed) {
        this.itemsSpeed = this.itemsSpeed + speed;
        System.out.println(itemsSpeed);
    }
    /**
     *Metoda vrací itemsSpeed nastavený pomocí setItemsSpeed volaný z CommandEat a CommandUse
     *
     * @return itemsSpeed
     */
    public double getItemsSpeed() {return itemsSpeed;}

    /**
     *Metoda je volaná z MainController z metody loadGame. Metoda projde všechny lokace
     *  a pokud jsou v nich nějaké předměty tak je odstraní
     *
     */
    public void deleteAllItems() {
        String[] areas = {"baziny","jeskyne","jeskyne_hlubiny","reka","stara_kovarna","srdce_lesa","stara_budova"};
        String[] items = {"vejce","obvazy","lekarnicka","brouky","boruvky","jedla_trava","zive_kvetiny","prvni_cast_klice"
        ,"druha_cast_klice","treti_cast_klice","ctvrta_cast_klice"};

        for(int i = 0 ; i < areas.length; i++){
            System.out.println(areas[i]);
            Area area = getCurrentAreaByName(areas[i]);
            for(int j = 0; j < items.length;j++) {
                if(area.containsItem(items[j])){
                    area.removeItem(items[j]);
                    System.out.println("item " + items[j] + " removed");
                }
            }
        }
    }

    /**
     *Metoda je volaná z MainController metody saveGame. Metoda prochází všechny oblasti hry a ukláda všechny předměty které nalezne
     * do pole a  oblast ve které předmět byl do druhého pole
     *
     * @return pole s předměty
     */
    public List<String> scanForItems() {
        String[] areas = {"baziny","jeskyne","jeskyne_hlubiny","reka","stara_kovarna","srdce_lesa","stara_budova"};
        String[] items = {"vejce","obvazy","lekarnicka","brouky","boruvky","jedla_trava","zive_kvetiny","prvni_cast_klice"
                ,"druha_cast_klice","treti_cast_klice","ctvrta_cast_klice"};

        List<String> areaItems = new ArrayList<String>();

        for(int i = 0 ; i < areas.length; i++){
            Area area = getCurrentAreaByName(areas[i]);
            for(int j = 0; j < items.length;j++) {
                if(area.containsItem(items[j])){
                    areaItems.add(items[j]);
                    areaForItem.add(areas[i]);
                    System.out.println("item " + items[j] + " added");
                    System.out.println("area " + areas[i] + " added");
                }
            }
        }
        return areaItems;
    }

    /**
     *Metoda vrací pole s lokacemi které je naplněno v metodě scanForItems
     *
     * @return pole s lokacemi
     */
    public List<String> getAreaForItem() {return areaForItem;}
}
