package cz.vse.grytsak;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Třída představuje atributy hlavní postavy Orsina. Ve třídě se nastavuje a propočítává jeho zdraví a rychlost
 * a také další funkce jako aktivita silového pole nebo postoj postavy. Třída se dá rovněž využít i pro výpis zmínených atributů.
 * 
 * @author Leon Grytsak
 * @version 1.0
 * 
 */
public class Atributes
{  
    private double health;
    private double speed;
    private double speedMax;
    private double speedItems;
    private boolean shield;
    private boolean shieldUsed;
    private boolean stance;
    /**
     * Konstruktor třídy. Nastaví hodnoty proměnným.
     */
    public Atributes()
    {
        health = 100;
        speed = 100;
        speedMax = 100;
        shield = false;
        shieldUsed = false;
        stance = false;
    }

    /**
     * Metoda vrací životy postavy zaokrouhlenou na 2 desetinná místa.
     * 
     * @return životy
     */
    public double getHealth()
    {
        return health = Math.round(health * 100.0) / 100.0;
    }

    /**
     * Metoda vrací rychlost postavy zaokrouhlenou na 2 desetinná místa.
     * 
     * @return rychlost
     */
    public double getSpeed()
    {
        return speed = Math.round(speed * 100.0) / 100.0;
    }

    /**
     * Metoda vrací stav štítu postavy.
     * 
     * @return stav štítu
     */
    public boolean getShield()
    {
        return shield;
    }

    /**
     * Metoda vrací stav proměnné getShieldUsed, která představuje zda byl štít již použit (štít se dá použít pouze 1x za hru).
     * 
     * @return stav proměnné getShieldUsed
     */
    public boolean getShieldUsed()
    {
        return shieldUsed;
    }

    /**
     * Metoda vrací postoj postavy.
     * 
     * @return postoj
     */
    public boolean getStance()
    {
        return stance;
    }

    /**
     * Metoda nastaví postoj postavy.
     * 
     * @param stanceType představuje typ postoje (true/false; skryty/normalni)
     */
    public void setStance(boolean stanceType)
    {
        stance = stanceType;
    }

    /**
     * Metoda nastaví stav štítu a zároveň nastaví shieldUsed na hodnotu true. shieldUsed se již nikdy nedá změnit na hodnotu false, tudíž nejde znova ve hře použít.
     *
     * @param stance představuje stav štítu (true - zapnutý, false - vypnutý)
     * @return
     */
    public void setShield(boolean stance)
    {
        shield = stance;
        shieldUsed = true;
    }

    /**
     * Metoda nastaví modifikátor pro výpočet rychlost speedItems.
     * 
     * @param modifier určuje hodnotu která se přidá k speedItems
     */
    public void setSpeedItems(double modifier)
    {
        speedItems = speedItems + modifier;
        setSpeed();
    }

    /**
     * Metoda nastaví životy postavy a ošetří zda jsou životy a rychlost vyšší či nižší než je min a max (0;100) a nastaví je na požadované hodnoty
     * zároveň také metoda vypočítá rychlost, protože se snižujícím zdravím se snižuje i rychlost postavy
     * 
     * @param modifier určuje o kolik se sniží/zvýší životy.
     */
    public void setHealth(double modifier)
    {
        health = health + modifier;
        if(health > 100) {

            health = 100;
        }
        if(health < 0) {

            health = 0;
        }
        speed = (speedMax - ((100-health)/1.8));
        if(speed > 100) {        
            speed = 100;
        }
        if(speed < 0) {       
            speed = 0;
        }
        speed = speed + speedItems;
        if(speed > 100) {        
            speed = 100;
        }
        if(speed < 0) {       
            speed = 0;
        }
    }

    /**
     * Metoda nastaví rychlost postavy a ošetří zda je rychlost vyšší či nižší než je min a max (0;100) a nastaví jí na požadované hodnoty 
     */
    public void setSpeed()
    {
        speed = (speedMax - ((100-health)/2.5));
        if(speed > 100) {        
            speed = 100;
        }
        if(speed < 0) {        
            speed = 0;
        }
        speed = speed + speedItems;
        if(speed > 100) {        
            speed = 100;
        }
        if(speed < 0) {       
            speed = 0;
        }
    }
}