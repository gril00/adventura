package cz.vse.grytsak;
/**
 * Třída slouží k zjískaní popisu předmětu a to ať se nacházi v oblasti nebo v inventáři
 * 
 * @author Leon Grytsak
 * @version 1.0
 */
public class CommandInspect implements ICommand
{
    private static final String NAME = "prozkoumej";

    private GamePlan plan;
    
    /**
     * Konstruktor třídy
     * 
     * @param plan přijímá aktuální stav hry z GamePlan a ukládá jej do proměnné plan
     */
    public CommandInspect(GamePlan plan)
    {
        this.plan = plan;
    }
    
    /**
     * Metoda vyhodnocuje průzkum předmětů pokdu zadáte správný a jeden parametr
     * Metoda vrátí popis předmětu
     * 
     * @param parameters název předmětu který chceme prozkoumat
     * @return výsledek průzkumu předmětu
     */
    public String process(String... parameters)
    {
        if (parameters.length == 0) {
            return "Nevím, co mám prozkoumat, musíš zadat název předmětu.";
        }

        if (parameters.length > 1) {
            return "Tomu nerozumím, neumím prozkoumat více předmětů současně.";
        }

        String itemName = parameters[0];
        Area area = plan.getCurrentArea();

        Inventory inventory = plan.getInventory();
        KeyInventory invK = plan.getKeyInventory();      

        if (area.containsItem(itemName)) {
            return area.getItem(itemName).getDescription();
        }

        if (inventory.containsItem(itemName)) {
            return inventory.getItem(itemName).getDescription();
        } 

        if(invK.keyParts() > 0) {
            return "Část klíče kterou potřebuju k sestavení klíče na otevření budovy s portálem.";
        }

        if (!area.containsItem(itemName)) {
            return "Předmět '" + itemName + "' tady není.";
        } 

        return "Předmět " + itemName + " v inventáři není.";
    }
    
    /**
     * Metoda vrací název příkazu
     * 
     * @return název příkazu
     */
    public String getName()
    {
        return NAME;
    }
}
