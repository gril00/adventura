package cz.vse.grytsak;

/**
 * Třída představuje inventář klíčů kam si postava ukládá části klíče, pro sebrání se používá stejný příkaz, ale části klíče se ukládají nezávisle na předmětech
 * v obyčejném inventáři, čili nezaplnují místo pro ostatní předměty.
 * 
 * @author Leon Grytsak
 * @version 1.0
 */
public class KeyInventory
{
    private int keyPart;
    private int repairedKey;  
    
    /**
     * Konstruktor třídy - nastaví hodnoty proměnným
     */
    public KeyInventory()
    {
        keyPart = 0;
        repairedKey = 0;
    }
    
    /**
     * Metoda slouží k přidání části klíče do inventáře klíčů. 
     */
    public void keyAdd()
    {
        keyPart = keyPart + 1; 
        if(repairedKey == 1){        
            keyPart = 0;
        }
    }
    
    /**
     * Metoda vrací počet častí klíču
     * 
     * @return počet částí klíčů
     */
    public int keyParts()
    {
        return keyPart;
    }
    
    /**
     * Metoda nastaví klíč na opravený
     */
    public void setRepairedKey()
    {
        repairedKey = repairedKey + 1; 
        keyPart = 0;
    }

    /**
     * Metoda vrací zda je klíč opravený
     * 
     * @return stav klíče (opravený - true; neopravený - false)
     */
    public boolean repairedKey()
    {
        if (repairedKey == 1) {       
            return true;
        }

        return false;
    }
}