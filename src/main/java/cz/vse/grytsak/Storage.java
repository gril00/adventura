package cz.vse.grytsak;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *Třída slouží k uchování všech prvků hry, třída je následně serializována do souboru save.ser pro uložení hry,
 * který se opět de-serializuje při načtení hry a 'vysype' zpět do proměnných či polí této třídy
 *
 *
 * @author Leon Grytsak
 * @version 2020
 */
public class Storage implements Serializable {
    String currentLocation;
    Double currentHealth;
    Double currentSpeed;
    List<String> areaItems= new ArrayList<String>();
    List<String> areaForItems = new ArrayList<String>();
    List<String> inventoryItems = new ArrayList<String >();
    Integer keys;
    boolean keyRepaired;
    boolean shieldState;
    boolean shieldUsed;
    boolean stanceState;

    public void setCurrentLocation(String currentLocation){
        this.currentLocation = currentLocation;
    }
    public String getCurrentLocation(){
        return currentLocation;
    }

    public Double getCurrentHealth() {return currentHealth;}
    public void setCurrentHealth(Double currentHealth) {this.currentHealth = currentHealth;}

    public Double getSpeedModifier() {return currentSpeed;}
    public void setSpeedModifier(Double currentSpeed) {this.currentSpeed = currentSpeed;}

    public void setAreaItems(String item) {
        areaItems.add(item);
    }
    public List<String> getAreaItems() {return areaItems;}

    public void setAreaForItems(String area) {
        areaForItems.add(area);
    }
    public List<String> getAreaForItems() {return areaForItems;}

    public void setInventoryItems(String item){
    inventoryItems.add(item);
    }
    public List<String> getInventoryItems() {return inventoryItems;}

    public void setKeys(Integer keys) {
        this.keys = keys;
    }
    public Integer getKeys() {return keys;}

    public void setKeyRepaired(boolean state){
        this.keyRepaired = state;
    }
    public boolean getKeyRepaired() {return keyRepaired;}

    public void setShieldState(boolean state){
        this.shieldState = state;
    }
    public boolean getShieldState() {return shieldState;}

    public void setShieldUsed(boolean state){
        this.shieldUsed = state;
    }
    public boolean getShieldUsed() {return shieldUsed;}

    public void setStanceState(boolean state){
        this.stanceState = state;
    }
    public boolean getStanceState() {return stanceState;}




}
