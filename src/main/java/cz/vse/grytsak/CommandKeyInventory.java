package cz.vse.grytsak;

/**
 * Třída slouží pro výpiš počtu částí klíčů v inventáři klíčů
 * 
 * @author Leon Grytsak
 * @version 1.0
 */
public class CommandKeyInventory implements ICommand
{
    private static final String NAME = "inventar_klicu"; 
    private GamePlan plan;

    /**
     * Kontruktor třídy
     * 
     * @param plan přijímá aktualní stav hry z GamePlan a ukládá do proměnné plan
     */
    public CommandKeyInventory(GamePlan plan)
    {
        this.plan = plan;    
    }    

    /**
     * Metoda slouží k vypsání počtu částí klíčů v inventáři a nebo zda má hráč opravený klíč
     * 
     * @param parameters může příjmout ale nevyužívá je
     * @return vrací stav inventáře klíčů
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length > 0) {
            return "K tomuto příkazu nezadávejte parametry.";
        }
        
        if (plan.getKeyInventory().repairedKey()) {        
            return "V inventáři částí klíčů je opravený klíč";
        }

        if (plan.getKeyInventory().keyParts() == 0 & !plan.getKeyInventory().repairedKey()) {        
            return "V inventáří částí klíčů žádné části klíče nejsou.";
        }
        return "V inventáři částí klíčů je: " + plan.getKeyInventory().keyParts() + " části klíčů.";

    }

    /**
     * Metoda vrací název příkazu
     * 
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }
}