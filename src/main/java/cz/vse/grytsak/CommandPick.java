package cz.vse.grytsak;

/**
 * Třída představuje příkaz seber, kterým hráč zvedá předměty z lokací a ukládá si je do inventáře
 * 
 * @author Leon Grytsak
 * @version 1.0
 */
public class CommandPick implements ICommand
{
    private static final String NAME = "seber";

    private GamePlan plan;
    private Inventory inventory;
    private KeyInventory invK;

    /**
     * Konstruktor třídy
     * 
     * @param plan - přijímá aktualní stav hry z GamePlan a ukládá ho do proměnné GamePlan plan
     */
    public CommandPick(GamePlan plan)
    {
        this.plan = plan;
        this.inventory = plan.getInventory();
        this.invK = plan.getKeyInventory();
    }

    /**
     * Metoda vyhodnocuje zvednutí předmětu
     * Pokud je parametr zadán špatně, metoda vrátí textový řetězec o tom že neví jaký předmět má sebrat
     * Pokud je zadáno víc parametrů, metoda vrátí textový řetězec o tom že neumí použít více předmětů najednou
     * Pokud je parametr zadán správně metoda kontroluje zda se předmět skutečně nachází v dané lokaci ve které hráč je
     * Pokud se tam předmět nachází ale nejde zvednou (heavy == true), metoda vrátí textový řetězec o tom že se předmět nedá zvednou
     * Pokud se předmět dá zvednout metoda uloží předmět do inventáře a odstraní ho z dané lokace
     * Pokud se jedná o část klíče, předmět je uložen do inventáře klíčů
     * Nakonec metoda zobrazí zbylé předmět, které se v lokaci ještě nacházejí, pokud tam nějake jsou.
     * 
     * @param parameters název předmětu co chceme sebrat
     * @return výsledek sbírání předmětu
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length == 0) {
            return "Orsin: Nevím, co mám sebrat, musíš zadat název předmětu.";
        }

        if (parameters.length > 1) {
            return "Orsin: Tomu nerozumím, neumím sebrat více předmětů současně.";
        }

        String itemName = parameters[0];
        Area area = plan.getCurrentArea();

        if (!area.containsItem(itemName)) {
            return "Orsin: Předmět '" + itemName + "' tady není. Asi jsi ho už sebral anebo tu nikdy nebyl.";
        }

        Item item = area.getItem(itemName);
        if(item.heavy()) {        
            return "Orsin: Předmět není možné sebrat, protože je příliš těžký.";
        }

        if(inventory.notFull() & item.notKeyPart() & !item.heavy()) {         
            inventory.itemAdd(item);
            area.removeItem(item.getName());
            return "Orsin: Sebral jsem předmět " + itemName + "' a uložil jsi ho do inventáře.\n" + plan.getCurrentArea().getItemsInArea();

        }

        if(!item.notKeyPart()) {        
            invK.keyAdd();
            area.removeItem(item.getName());   
            return "Orsin: Sebral jsem část klíče a uložil si jí do inventáře. Nyní mám " + invK.keyParts() + " část/i klíče.\n" + plan.getCurrentArea().getItemsInArea();   
        }

        
        return "Inventář je plný.";
    }

    /**
     * Metoda vrátí název příkazu
     * 
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }

}
