package cz.vse.grytsak;

/**
 * Třída slouží k položení předmětu z inventáře do lokace
 * 
 * @author Leon Grytsak
 * @version 1.0
 */
public class CommandLay implements ICommand
{
    private static final String NAME = "poloz"; 
    private GamePlan plan;
    
    /**
     * Konstruktor třídy
     * 
     * @param plan přijímá aktuální stav hry z GamePlan a ukládá jej do proměnné plan
     */
    public CommandLay(GamePlan plan)
    {
        this.plan = plan;    
    }
    
    /**
     * Metoda vyhodnocuje zvednutí předmětu
     * Pokud hráč zadá špatný parametr, metoda vrátí textový řetězec o tom že hřáč zadal špatný parametr
     * Pokud hráč zadá více parametrů, metoda vrátí textový řetězec o tom že metoda neumí zpracovat více parametrů
     * Pokud hráč zadá správný parametr předmět se položí do lokace ve které hráč je
     * 
     * @param parameters název předmětu který chcete položit
     * @return výsledek pokládání předmětu
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length == 0) {
            return "Orsin: Nevím, co mám položit, musíš zadat název předmětu.";
        }

        if (parameters.length > 1) {
            return "Orsin: Tomu nerozumím, neumím položit více předmětů současně.";
        }

        String itemName = parameters[0];
        Area area = plan.getCurrentArea();
        Inventory inv = plan.getInventory();
        Item deleted = inv.getItem(itemName);

        if(deleted == null) {        
            return "Orsin: Věc není v batohu.";
        }
        else {        
            area.addItem(deleted);
            plan.getInventory().deleteItem(itemName);
        }

        return "Položil jsi předmět " + itemName + "\n" + plan.getCurrentArea().getItemsInArea();
    }

    /**
     * Metoda vrací název příkazu
     * 
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }   
}