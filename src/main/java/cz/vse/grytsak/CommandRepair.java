package cz.vse.grytsak;

/**
 * Třída slouží k opravení klíče
 */
public class CommandRepair implements ICommand
{
    private static final String NAME = "oprav_klic"; 
    private GamePlan plan;

    /**
     * Konstruktor třídy
     * 
     * @param plan přijímá aktualní stav hry z GamePlan a ukládá ho do proměnné plan
     */
    public CommandRepair(GamePlan plan)
    {
        this.plan = plan;    
    }

    /**
     * Metoda slouží k opravení klíče
     * Když herní postava nemá 4 části klíče a není v lokaci kovárna, klíče není možné opravit a metoda nato upozorní
     * Pokud herní postava splňuje požadavky pro opravu klíče, klíč se opraví
     * 
     * @param parameters přijímá ale nepoužívá
     * @return výsledek opravy klíče
     */
    @Override
    public String process(String... parameters)
    {
        if (parameters.length > 0) {
            return "K tomuto příkazu nezadávejte parametry.";
        }
        
        if(plan.getCurrentArea().equals(plan.getSmeltery()))
        {
            if(plan.getKeyInventory().keyParts() == 4) {            
                plan.getKeyInventory().setRepairedKey();
                return "Orsin: Klíč byl opraven, teď mužů otevřít dveře ve staré budově a dostat se ke svému portálu.";
            }
            else {            
                return "Orsin: Nemám dostatek částí na opravu klíče.";
            }
        }

        return "Orsin: Nejsem na správném místě!";
    }

    /**
     * Metoda vrací název příkazu
     * 
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return NAME;
    }   
}