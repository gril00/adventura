package cz.vse.grytsak;



/**
 * Třída představuje předměty ve hře a jejich atributy
 * 
 * @author Leon Grytsak, Jan Říha
 * @version 1.0
 */
public class Item
{
    private String name;
    private String description;
    private boolean notKeyPart;
    private boolean heavy;

    /**
     * Kontstruktor třídy
     * 
     * @param name jméno předmětu
     * @param description popis předmětu
     * @param notKeyPart říká jestli item není nebo je část klíče (true,false)
     * @param heavy říka jestli se item dá zvednout nebo nedá (true, false)
     */
    public Item(String name, String description, boolean notKeyPart, boolean heavy)
    {
        this.name = name;
        this.description = description;
        this.notKeyPart = notKeyPart;
        this.heavy = heavy;
    }

    /**
     * Konstruktor třídy - nastavuje defaultní atributy itemu notKeyPart a heavy
     * 
     * @param name příjme a nastaví jméno itemu
     * @param description příjme a nastaví popis itemu
     */
    public Item(String name, String description)
    {
        this(name, description,true, false);
    }

    /**
     * Metoda vrací název itemu
     * 
     * @return název itemu
     */
    public String getName()
    {
        return name;
    }
    /**
     * Metoda vrací popis itemu
     * 
     * @return popis itemu
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
     * Metoda nastaví popis itemu
     * 
     * @param description představuje popis itemu
     */
    public void setDescription(String description)
    {
        this.description = description;
    }
    
    /**
     * Metoda vrací zda item není část klíče
     * 
     * @return část klíče (true,false)
     */
    public boolean notKeyPart()
    {
        return notKeyPart;
    }
    
    /**
     * Metoda vrací zda item není těžký
     * 
     * @return těžký (true,false)
     */
    public boolean heavy()
    {
        return heavy;
    }
}
