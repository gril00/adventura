package textui;

import com.sun.javafx.geom.Point2D;
import cz.vse.grytsak.*;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import start.Start;

import java.io.*;
import java.net.URL;
import java.util.*;


/**
 * Třída funguje jako ovladač hlavní scény.
 * V třídě jsou deklarovány a použity všechny prvky z scene.fxml uložené v resources.
 * Třída přijíma ze Start aktuální stav hry v metodě init a ta dále volá všechny ostatní metody které přiřadí funcki ovládacím prvkům scény.
 * Od Start třída zároveň dostává Stage primaryStage pro použití při ukončení aplikace.
 * Metoda help této třídy vytváří další samostatné fx okno které zpracovává soubor napoveda.html.
 * Metody třídy také zpracovávají manuálně zadané příkazy z prvku TextField.
 * V této třídě jsou také metody saveGame a loadGame které zpracovávají data ukládají je do třídy Storage, která je následně serializována
 * a desirializována podle potřeby.
 *
 * @author Leon Grytsak
 * @version 2020
 */
public class MainController {
    public IGame game;
    public TextArea textOutput;
    public TextField textInput;

    public javafx.scene.control.Label locationDesc;

    public VBox exits;
    public VBox items;
    public VBox inventory;
    public ProgressBar pb_health;
    public ProgressBar pb_speed;
    public Label key_parts;
    public Label hp_text;
    public Label speed_text;
    public Button repair_key;
    public Button shield_button;
    public Circle ball;
    public ImageView gamer;
    public ImageView shield_image;
    public MenuItem close_app;
    public MenuItem new_game;
    public MenuItem help;
    public MenuItem saveGame;
    public MenuItem loadGame;
    public ToggleButton stance1;
    public ToggleButton stance2;
    public ImageView stance_image;
    public ComboBox select_command;
    public Button confirm_selected_command;

    private Map<String, Point2D> coordinates = new HashMap();

    /**
     * Metoda přijímá aktuální stav hry a volá další metody v MainController které nastaví určité ovládací prvky, které se
     * nemusí obnovovat při každé změně lokace ve hře.
     * Také volá createCoordinates ze které se do proměnné coordinates nahrajou všechny koordináty potřebné pro mapu hry.
     *
     * @param game aktuální stav hry
     */
    public void init(IGame game) {
        this.game = game;
        coordinates = createCoordinates();
        stance1.setSelected(true);
        update();
        endApp();
        help();
        saveGame();
        loadGame();
    }

    /**
     * Metoda slouží k aktualizaci většiny ovládacích prvků a iformačních prvků jako jsou progressbary pro životy,
     * metoda také zastavuje hru pokud je gameOver pravda a vypíše proč hra skončila(hrač prohrál - smrt na hlad),
     * metoda také nastavuje pozici obrázku hráče na obrázku mapy podle souřadnic zjískaných z proměnné coordinates
     * a vypisuje aktuální lokaci + její popis a předměty, východy.
     */
    private void update()  {
         if(game.isGameOver()) {
             shield_image.visibleProperty().setValue(false);
             if (game.getGamePlan().isDefeated()) {
                 InputStream stream = getClass().getClassLoader().getResourceAsStream("gamer_dead.jpg");
                 Image img = new Image(stream);
                 gamer.setImage(img);
                 String deathText = null;
                 if(getMonster().getMonsterAttacked() & getMonster().getDamage() >= getAtributes().getHealth()) {
                     deathText = " Tvoje postava umřela na útok příšery.\n";
                 }else if(!getMonster().getMonsterAttacked()){
                     deathText = " Tvoje postava umřela na hlad.\n";
                 } else {
                     deathText = " Tvoje postava umřela na útok příšery.\n";
                 }
                 updateAtributes();
                 textOutput.appendText("Prohrál jsi." + deathText);
                 locationDesc.setText("Prohrál jsi." + deathText +
                         "Můžeš buď začít novou hru nebo jí ukončit v MENU.\n");
             } else if (game.getGamePlan().isVictorious()) {
                 updateAtributes();
                 textOutput.appendText("Vyhrál jsi.");
                 locationDesc.setText("Vyhrál jsi.\n" +
                         "Můžeš buď začít novou hru nebo jí ukončit v MENU.\n");}
              }  else if(!game.isGameOver()){
                    String description = getGamePlan().getFullDescription();
                    locationDesc.setText(description);
                    String currentLocation = getGamePlan().getName();

                    gamer.setLayoutX(coordinates.get(currentLocation).x+3);
                    gamer.setLayoutY(coordinates.get(currentLocation).y);
                    shield_image.setLayoutX(coordinates.get(currentLocation).x);
                    shield_image.setLayoutY(coordinates.get(currentLocation).y-22);
                    updateAtributes();
                    updateExits();
                    updateItems();
                    updateKeyInventory();
                    updateShield();
                    updateStance();
                    updateSelectCommand();
                }
    }
    /**
     * Metoda slouží k aktualizaci východů graficky.
     * Metoda zjíská data o východech z Area a uloží je do pole.
     * metoda nastavuje východům také obrázky a akci která se stane pokud se na obrázek či text názvu lokace klikne.
     */
    private void updateExits() {
        Collection<Area> exitList = getGamePlan().getExits();
        exits.getChildren().clear();
            for (Area area : exitList) {
                String exitName = area.getName();
                Label exitLabel = new Label(exitName);
                exitLabel.setCursor(Cursor.HAND);
                exitLabel.setTooltip(new Tooltip(area.getFullDescription()));

                InputStream stream = getClass().getClassLoader().getResourceAsStream(exitName + ".jpg");
                Image img = new Image(stream);
                ImageView imageView = new ImageView(img);
                imageView.setFitWidth(60);
                imageView.setFitHeight(40);
                exitLabel.setGraphic(imageView);


                exitLabel.setOnMouseClicked(event -> {
                    if(!game.isGameOver()) {
                    executeCommand("jdi " + exitName);}
                });

                exits.getChildren().add(exitLabel);

            }
    }

    /**
     * Metoda aktualizuje grafické předměty v lokaci.
     * Metoda zjíská data o předmětech z Area a uloží je do pole
     * Metoda nastavuje předmětům v lokaci také obrázky a nastavuje akci která se stane po kliknutí na obrázek/text předmětu.
     */
    private void updateItems() {
        Collection<Item> itemList = getGamePlan().getItemList().values();
        items.getChildren().clear();

        for(Item item: itemList) {
            String itemName = item.getName();
            Label itemLabel = new Label(itemName);
            InputStream stream = getClass().getClassLoader().getResourceAsStream(itemName + ".jpg");
            Image img = new Image(stream);
            ImageView imageView = new ImageView(img);
            imageView.setFitWidth(60);
            imageView.setFitHeight(40);
            itemLabel.setGraphic(imageView);

            if(!item.heavy()) {
                itemLabel.setCursor(Cursor.HAND);

                itemLabel.setOnMouseClicked(event ->  {
                    executeCommand("seber "+itemName);
                    updateInventory();
                });

            } else {
               itemLabel.setTooltip(new Tooltip("Tuto věc nejde sebrat, protože je příšliš těžká."));

            }

            items.getChildren().add(itemLabel);

        }
    }

    /**
     * Metoda aktualizuje grafický inventář.
     * Metoda zjíská data o inventáři z inventáře a uloží je do pole.
     * Předmětům je přidělen obrázek a kontextové menu které po pravém kliku dává možnost předmět položit a nebo sníst/použít
     * Zároveň je přidělen i toolTip
     * žádnou z akcí na kliknutí není možné použít pokud hra skončila (isGameOver = true)
     */
    private void updateInventory() {
        ArrayList<Item> inventoryList = getInventory().getInventoryItems();
        inventory.getChildren().clear();

        for(Item item: inventoryList) {
            String itemName = item.getName();
            Label itemLabel = new Label(itemName);
            InputStream stream = getClass().getClassLoader().getResourceAsStream(itemName + ".jpg");
            Image img = new Image(stream);
            ImageView imageView = new ImageView(img);
            imageView.setFitWidth(60);
            imageView.setFitHeight(40);
            itemLabel.setGraphic(imageView);

            ContextMenu ctxtMenu = new ContextMenu();
            MenuItem choice1 = new MenuItem("Sněz/Použij");
            MenuItem choice2 = new MenuItem("Polož");
            ctxtMenu.getItems().addAll(choice1,choice2);

            itemLabel.setContextMenu(ctxtMenu);
            itemLabel.setCursor(Cursor.HAND);

            Tooltip tooltp = new Tooltip("Klikni pravým pro výběr akce.");
            Tooltip.install(itemLabel,tooltp);

            inventory.getChildren().add(itemLabel);

            choice1.setOnAction(event -> {
                if(itemName == "obvazy" || itemName == "lekarnicka") {
                    if(!game.isGameOver()){
                    executeCommand("pouzij "+ itemName);
                    inventory.getChildren().clear();
                    updateAtributes();
                    updateInventory();}
                }
                else if(itemName != "obvazy" || itemName != "lekarnicka") {
                    if(!game.isGameOver()){
                    executeCommand("snez "+ itemName);
                    inventory.getChildren().clear();
                    updateAtributes();
                    updateInventory();}
                }
            });

            choice2.setOnAction(event -> {
                if(!game.isGameOver()) {
                executeCommand("poloz "+ itemName);
                inventory.getChildren().clear();
                updateItems();
                updateInventory();}
            });

        }


    }

    /**
     * Metoda aktualizuje grafické atributy hráče (životy a rychlost).
     * metoda zjíská data z atributů a uloží je do proměných, dále vhodně nastaví hodnoty pro progressbar
     */
    private void updateAtributes() {
        double health = getAtributes().getHealth();
        double speed = getAtributes().getSpeed();

        hp_text.setText("Životy: " + health);
        speed_text.setText("Rychlost: " + speed);
        pb_health.setProgress(health / 100);
        pb_speed.setProgress(speed / 100);
    }

    /**
     * Metoda graficky aktualizuje inventář klíčů.
     * metoda uloží počet klíčů z inventáře do promenné a dále upraví Label v rozhraní na tuto hodnotu.
     * Pokud má hráč opravený klíč metoda Label zmení na tento stav
     */
    private void updateKeyInventory() {
        int keys = getKeyInv().keyParts();

        repair_key.setOnAction(event -> {
            if(keys == 4 & game.getGamePlan().getCurrentArea().equals(game.getGamePlan().getSmeltery())) {
                executeCommand("oprav_klic");
                updateKeyInventory();
            }
        });

        if(getKeyInv().repairedKey()){
        String resultText = "Mám opravený klíč!";
        key_parts.setText(resultText);
        }else{
        String resultText = "Části klíče: " + keys;
        key_parts.setText(resultText);}
    }

    /**
     * Metoda aktualizuje stav štítu a přiděluje tlačítku akci na kliknutí -> použíj příkat  'stit'.
     * Stav štítu je zjískán z Atributes.
     * Zároveň zviditelnuje a nebo zneviditelní obrázek představující štít na herní mapě na pozici hráče.
     * Také mění barvu objektu vedle tlačíka na zelenou pokud je štít zaplný a nebo červenou pokud je vyplý
     */
    private void updateShield() {
        boolean shield = getAtributes().getShieldUsed();
        if(!shield){
            shield_button.setOnAction(event -> {
                executeCommand("stit");
            });
        }
        if(!getAtributes().getShield()){
            shield_image.visibleProperty().setValue(false);
            ball.setFill(javafx.scene.paint.Color.RED);
        } else {
            shield_image.visibleProperty().setValue(true);
            ball.setFill(Color.GREEN);
        }
    }

    /**
     * Metoda aktualizuje postoj hráče.
     * Nastavování postoje je provedeno pomocí toggle buttonu
     * Metoda zároveň aktualizuje obrázek hráče na mapě a obrázek postavičky vedle tlačítek dle stavu postoje
     */
    private void updateStance() {
        final ToggleGroup tgGroup = new ToggleGroup();
        stance1.setToggleGroup(tgGroup);
        stance2.setToggleGroup(tgGroup);
        if(getAtributes().getStance())
        {
            stance2.setSelected(true);
            InputStream stream = getClass().getClassLoader().getResourceAsStream( "stance_crouch.png");
            Image img = new Image(stream);
            stance_image.setImage(img);
            stream = getClass().getClassLoader().getResourceAsStream( "gamer_crouch.jpg");
            img = new Image(stream);
            gamer.setImage(img);
        }else {
            stance1.setSelected(true);
            InputStream stream = getClass().getClassLoader().getResourceAsStream( "stance_normal.png");
            Image img = new Image(stream);
            stance_image.setImage(img);
            stream = getClass().getClassLoader().getResourceAsStream( "gamer.jpg");
            img = new Image(stream);
            gamer.setImage(img);
        }
        tgGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if(newValue == stance1){
                    executeCommand("postoj normalni");
                    InputStream stream = getClass().getClassLoader().getResourceAsStream( "stance_normal.png");
                    Image img = new Image(stream);
                    stance_image.setImage(img);
                }
                if(newValue == stance2) {
                    executeCommand("postoj skryty");
                    InputStream stream = getClass().getClassLoader().getResourceAsStream( "stance_crouch.png");
                    Image img = new Image(stream);
                    stance_image.setImage(img);
                }
                if(newValue == null) {
                    oldValue.setSelected(true);
                }
            }
        });
    }

    /**
     * Metoda aktualizuje seznam příkazů momentálně použitelných pro comboBox.
     * Metoda postupně zjískává data z několika tříd a postupně je používá při tvoření seznamu.
     * Metoda do seznamu ukláda pouze to co se dá momentálně použít, takže pokud jste například použili štít, již ho na tomto
     * seznamu neuvidíte.
     * Po vybrání příkazu stačí kliknout na tlačítko Potvrdit Příkaz a ten se dále odešle do metody executeCommand
     * combobox má také defaultní hodnotu kterou lze bez výběru ihned potvrdit
     */
    private void updateSelectCommand() {
        ObservableList<String> options = FXCollections.observableArrayList();

        if(getKeyInv().keyParts() == 4 & game.getGamePlan().getCurrentArea().equals(game.getGamePlan().getSmeltery())) {
            options.add("oprav_klic");
        }

        Collection<Area> exitList = getGamePlan().getExits();

        for (Area area : exitList) {
            String exitName = area.getName();
            options.add("jdi " + exitName);
        }

        Collection<Item> itemList = getGamePlan().getItemList().values();

        for(Item item: itemList) {
            String itemName = item.getName();
            options.add("seber " + itemName);
        }

        ArrayList<Item> inventoryList = getInventory().getInventoryItems();

        for(Item item: inventoryList) {
            String itemName = item.getName();
            options.add("poloz " + itemName);

            if(itemName != "obvazy" & itemName != "lekarnicka") {
                options.add("snez "+itemName);
            }
            else{
                options.add("pouzij " + itemName);
            }
        }

        options.add("konec");
        options.add("napoveda");
        options.add("postoj normalni");
        options.add("postoj skryty");
        if(!getAtributes().getShieldUsed()) {
        options.add("stit");}

        select_command.setItems(options);
        select_command.setVisibleRowCount(8);
        select_command.getSelectionModel().selectFirst();
        final String[] selected_command = new String[1];
        select_command.getSelectionModel().selectedItemProperty()
                .addListener(new ChangeListener<String>() {
                    public void changed(ObservableValue<? extends String> observable,
                                        String oldValue, String newValue) {
                        selected_command[0] = newValue;
                    }
                });
        confirm_selected_command.setOnAction(event -> {
            if(selected_command[0] != null) {
                    executeCommand(selected_command[0]);
                    updateInventory();
            } else {
                executeCommand((String) select_command.getValue());
            }
        });
    }

    /**
     * Metoda slouží k nastavení koordinátu na obrázek mapy pro každou lokaci ve hře
     *
     * @return coordinates vrací pole s hodnotami
     */
    private Map<String, Point2D> createCoordinates() {
        Map<String, Point2D> coordinates = new HashMap<>();
        coordinates.put("vchod_do_lesa", new Point2D(48,40));
        coordinates.put("baziny", new Point2D(48,138));
        coordinates.put("jeskyne", new Point2D(48,238));
        coordinates.put("jeskyne_hlubiny", new Point2D(48,335));
        coordinates.put("reka", new Point2D(220,138));
        coordinates.put("stara_kovarna", new Point2D(380,138));
        coordinates.put("srdce_lesa", new Point2D(220,238));
        coordinates.put("stara_budova", new Point2D(220,335));

        return coordinates;
    }

    /**
     * Metoda ukončí aktuální hru a začne novou
     *
     * @param primaryStage přijímá primaryStage ze metody recieveStage
     */
    private void startGame(Stage primaryStage){
        new_game.setOnAction(event -> {
            primaryStage.close();
            Platform.runLater(() -> {
                try {
                    new Start().start(new Stage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        });

    }

    /**
     * Metoda ukončí hru a zavře aplikaci
     */
    private void endApp() {
        close_app.setOnAction(event -> {
            Platform.exit();
            System.exit(0);
        });
    }

    /**
     * Metoda slouží k vytvoření další scény a zobrazení nápovědy z html souboru.
     * Metoda vytvoří vlastní scénu a kontejner VBox do kterého se vloží prvek webView který představuje prohlížecí okno
     * do kterého se nahraje html soubor.
     */
    private void help() {
        help.setOnAction(event -> {
            try {
                Stage stage = new Stage();
                stage.setTitle("Nápověda");
                WebView webView = new WebView();
                WebEngine engine = webView.getEngine();
                File file = new File("src/main/resources/napoveda.html");
                URL url = file.toURI().toURL();
                engine.load(url.toString());
                VBox root = new VBox();
                root.getChildren().addAll(webView);
                webView.setMinHeight(800);
                stage.setScene(new Scene(root,650,850));
                stage.setMaxHeight(850);
                stage.setMinHeight(850);
                stage.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Metoda slouží k uložení hry do souboru save.ser pomocí serializace třídy Storage.
     * Metoda uloží celkový stav hry do třídy storage a následně jí serializuje.
     */
    private void saveGame() {
        Storage storage = new Storage();
        saveGame.setOnAction(event -> {
            storage.setCurrentLocation(game.getGamePlan().getCurrentArea().getName());
            storage.setCurrentHealth(getAtributes().getHealth());
            storage.setSpeedModifier(game.getGamePlan().getItemsSpeed());
            List<String> areaItems = new ArrayList<String>(game.getGamePlan().scanForItems());
            List<String> areaForItems = new ArrayList<String>(game.getGamePlan().getAreaForItem());
            System.out.println(areaItems.size());
            for(int i = 0; i < areaItems.size();i++) {
                String itemName = areaItems.get(i);
                storage.setAreaItems(itemName);
                String areaName = areaForItems.get(i);
                storage.setAreaForItems(areaName);
            }
            ArrayList<Item> inventoryList = getInventory().getInventoryItems();

            for(int i = 0; i < inventoryList.size();i++) {
            String itemName = inventoryList.get(i).getName();
            storage.setInventoryItems(itemName);
            }

            storage.setKeys(getKeyInv().keyParts());
            storage.setKeyRepaired(getKeyInv().repairedKey());

            storage.setShieldState(getAtributes().getShield());
            storage.setShieldUsed(getAtributes().getShieldUsed());
            storage.setStanceState(getAtributes().getStance());

            try{
                FileOutputStream fileOut = new FileOutputStream("src/main/resources/save.ser");
                ObjectOutputStream outStream = new ObjectOutputStream(fileOut);
                outStream.writeObject(storage);
                outStream.close();
            } catch (Exception exc){
                System.out.println(exc);
            }
        });
    }

    /**
     * Metoda slouží k načtení hry ze souboru save.ser který obsahuje serializovanou třídu Storage.
     * Metoda třídu nejprve deserializuje a poté využije data v ní pro nastavení herního prostředí.
     * Metoda nastavuje akci po položku v menu.
     * Tato akce nelze použít pokud je hra v stavu gameOver.
     */
    private void loadGame() {
        loadGame.setOnAction(event -> {
            try{
                if(!game.isGameOver()){
                Storage storage = null;
                FileInputStream fileIn = new FileInputStream("src/main/resources/save.ser");
                ObjectInputStream in = new ObjectInputStream(fileIn);
                storage = (Storage) in.readObject();
                in.close();
                fileIn.close();

                String currentArea = storage.getCurrentLocation();
                Area area = game.getGamePlan().getCurrentAreaByName(currentArea);
                game.getGamePlan().setCurrentArea(area);

                Double itemsSpeed = storage.getSpeedModifier();
                getAtributes().setSpeedItems(itemsSpeed);

                Double currentHealth = storage.getCurrentHealth();
                getAtributes().setHealth(-(100-currentHealth));

                game.getGamePlan().deleteAllItems();

                List<String> areaItems = new ArrayList<String>(storage.getAreaItems());
                List<String> areaForItems = new ArrayList<String>(storage.getAreaForItems());

                for(int i = 0; i < areaItems.size();i++) {
                    String areaItem = areaItems.get(i);
                    Item item = game.getGamePlan().getItemByName(areaItem);
                    Area areaForItem = game.getGamePlan().getCurrentAreaByName(areaForItems.get(i));
                    areaForItem.addItem(item);
                }

                List<String> inventoryItem = new ArrayList<String>(storage.getInventoryItems());
                System.out.println(inventoryItem);
                for(int i = 0; i < inventoryItem.size();i++) {
                    String itemName = inventoryItem.get(i);
                    Item item = game.getGamePlan().getItemByName(itemName);
                    System.out.println(item);
                    getInventory().itemAdd(item);
                }
                System.out.println(storage.getKeys());
                while(storage.getKeys() > getKeyInv().keyParts()){
                    getKeyInv().keyAdd();
                }
                if(storage.getKeyRepaired()){
                    getKeyInv().setRepairedKey();
                }

                if(storage.getShieldUsed()){
                    getAtributes().setShield(false);
                    getAtributes().setShield(storage.getShieldState());
                }

                getAtributes().setStance(storage.getStanceState());

                updateInventory();
                update();} else {
                    locationDesc.setText("Nejdříve začni novou hru.");
                    textOutput.appendText("Nejdříve začni novou hru.\n");

                }
            }catch (Exception exc) {
                System.out.println(exc);
            }
        });
    }

    private Area getGamePlan() {return game.getGamePlan().getCurrentArea();}
    private Inventory getInventory() {return game.getGamePlan().getInventory();}
    private Atributes getAtributes() {return game.getGamePlan().getAtributes();}
    private KeyInventory getKeyInv() {return game.getGamePlan().getKeyInventory();}
    private Monster getMonster() {return game.getGamePlan().getMonster();}

    /**
     * Metoda slouží k vykonání příkazu buď zadného ručně do textového pole a nebo příkazu posledného z jakékoliv jiné metody
     * Metoda také zprácovává situaci kdy uživatel ručně zadá příkaz 'konec'
     * Metoda aktualizuje textOutput a locationDesc z dat které dostala po vykonání příkazu
     * Metoda nedělá nic pokud isGameOver = true
     *
     * @param command příkaz poslaný do metody
     */
    private void executeCommand(String command) {
        if(command.equals("konec") & !game.isGameOver()) {
            game.processCommand("konec");
            textOutput.appendText("Ukončil jsi hru příkazem 'konec'\n");
            locationDesc.setText("Ukončil jsi hru příkazem 'konec'\n" +
                    "Začni novou hru nebo ukonči hru v MENU.");
            update();
        } else if(!game.isGameOver()){
            String result = game.processCommand(command);
            textOutput.appendText(result + "\n");
            update();}
    }

    /**
     * Metoda zpracuje vstupní klávesu a pokud se jedná o ENTER pošle udaje z textInput do executeCommand
     *
     * @param keyEvent klávesa kterou uživatel zmáčknul na klávesnici
     */
    public void onInputKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            executeCommand(textInput.getText());
            textInput.setText("");
            updateInventory();
        }
    }

    /**
     * Metoda přijímá primaryStage ze startu a posílá ho metodě startGame
     *
     * @param primaryStage primární scéna poslaná z třídy Start
     */
    public void receiveStage(Stage primaryStage){ startGame(primaryStage); }
}
