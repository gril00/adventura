package start;

import cz.vse.grytsak.Game;
import cz.vse.grytsak.IGame;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import textui.MainController;
import textui.TextUI;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;


/**
 * Hlavní třída určená pro spuštění hry. Obsahuje pouze statickou metodu
 * {@linkplain #main(String[]) main}, která vytvoří instance logiky hry
 * a uživatelského rozhraní, propojí je a zahájí hru.
 *
 * @author Jarmila Pavlíčková
 * @author Jan Říha
 * @author Leon Grytsak
 * @version LS 2020
 */

public final class Start extends Application
{
    /**
     * Metoda pro spuštění celé aplikace.
     *
     * @param args parametry aplikace z příkazového řádku
     */
    public static void main(String[] args)
    {
        List<String> intro = Arrays.asList(args);
        
        if (intro.contains("text")) {
            IGame game = new Game();
            TextUI ui = new TextUI(game);
            ui.play();
        } else {
            launch();
        }
    }

    /**
     *Metoda nastavuje primární scénu grafického rozhraní, načítá strukturu scény ze souboru .fxml a zároveň načítá její ovladač
     * před zviditelněním scény se volá metoda init v MainController do které se zároven posílá aktuální stav hry
     * také odesílá primaryStage do MainController
     *
     * @param primaryStage primární scéna, Stage - třída javy
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
    primaryStage.setFullScreen(false);
    primaryStage.setHeight(1020);
    primaryStage.setWidth(725);
    primaryStage.setMaxWidth(725);
    primaryStage.setMaxHeight(1020);
    primaryStage.setMinHeight(1020);
    primaryStage.setMinWidth(725);
    primaryStage.setTitle("Orsinova dobrodružství - útěk ze záhadné planety");

        FXMLLoader loader = new FXMLLoader();
        InputStream stream = getClass().getClassLoader().getResourceAsStream("scene.fxml");
        Parent root = loader.load(stream);

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);

        MainController controller = loader.getController();
        IGame game = new Game();
        controller.init(game);
        primaryStage.show();

        controller.receiveStage(primaryStage);
    }
}


