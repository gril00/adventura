
package src.test.java.cz.vse.grytsak;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import cz.vse.grytsak.CommandEat;
import cz.vse.grytsak.GamePlan;
import cz.vse.grytsak.Inventory;
import cz.vse.grytsak.Item;

import static org.junit.Assert.assertFalse;

/*******************************************************************************
 * Testovací třída {@code CommandEatTest} slouží ke komplexnímu otestování
 * třídy {@link CommandEatTest}.
 *
 * @author  Leon Grytsak
 * @version 1.0
 */
public class CommandEatTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }



    /***************************************************************************
     * Test of the {@link #setUp()} method preparing the test fixture.
     */
    @Test
    public void testCommandEat()
    {
        GamePlan plan = new GamePlan();
        CommandEat cmdEat = new CommandEat(plan);
        Item item = new Item("boruvky","dfsdfsdifunhhučřžgu");
        Inventory inventory = plan.getInventory();
        
        assertEquals(cmdEat.process(),"Orsin: Nevím co mám sníst, musíš zadat název předmětu.");
        assertEquals(cmdEat.process("sfsxdsf", "dfsd"),"Orsin: Tomu nerozumím, neumím sníst více předmětů najednou.");
        inventory.itemAdd(item);
        cmdEat.process("boruvky");
        assertFalse(inventory.containsItem("boruvky"));
        
        //Poznámka: CommandUse příkaz nebyl testován protože jeho funkcionalita je totožná s CommandEat
        //akorát mi bylo blbé nechat příkaz snez na např lekarnicka :)
    }

    private void assertEquals(String process, String s) {
    }

}
