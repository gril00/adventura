
package cz.vse.grytsak;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import cz.vse.grytsak.*;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída {@code CommandPickTest} slouží ke komplexnímu otestování
 * třídy {@link CommandPickTest}.
 *
 * @author  Leon Grytsak
 * @version 1.0
 */
public class CommandPickTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }


    /***************************************************************************
     * Metoda testuje uspěšné zvednutí předmětu.
     */
    @Test
    public void testPickUp()
    {        
        GamePlan plan = new GamePlan();
        CommandPick pick = new CommandPick(plan);
        Item item = new Item("predmet", "dsfsdfsf");
        Area area = plan.getCurrentArea();
        Inventory inventory = plan.getInventory();
        area.addItem(item);
        
        pick.process("predmet");
        assertEquals(inventory.containsItem("predmet"), true);
        
    }

}
