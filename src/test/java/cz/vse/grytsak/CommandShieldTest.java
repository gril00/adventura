package cz.vse.grytsak;


import cz.vse.grytsak.Atributes;
import cz.vse.grytsak.CommandShield;
import cz.vse.grytsak.GamePlan;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída {@code CommandShieldTest} slouží ke komplexnímu otestování
 * třídy {@link CommandShieldTest}.
 *
 * @author  Leon Grytsak
 * @version 1.0
 */
public class CommandShieldTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }



    /***************************************************************************
     * Metoda testuje třídu CommandShield
     */
    @Test
    public void testCommandShield()
    {
        GamePlan plan = new GamePlan();
        CommandShield cmdShield = new CommandShield(plan);
        Atributes atributes = plan.getAtributes();
        assertEquals(cmdShield.process("sfdfs"),"K tomuto příkazu nezadávejte parametry.");
        assertEquals(cmdShield.process(),"Orsin: Silové pole je zapnuté a ochraní tě před dalším útokem příšery.");
        atributes.setShield(true);
        assertEquals(cmdShield.process(),"Orsin: Štít jsem už použil!");
    }

}
