package cz.vse.grytsak;


import cz.vse.grytsak.Atributes;
import cz.vse.grytsak.CommandStance;
import cz.vse.grytsak.GamePlan;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída {@code CommandStanceTest} slouží ke komplexnímu otestování
 * třídy {@link CommandStanceTest}.
 *
 * @author  Leon Grytsak
 * @version 1.0
 */
public class CommandStanceTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }



    /***************************************************************************
     * Metoda testuje třídu CommandStance
     */
    @Test
    public void testCommandStance()
    {
        GamePlan plan = new GamePlan();
        CommandStance cmdStance = new CommandStance(plan);
        Atributes atributes = plan.getAtributes();
        assertEquals(cmdStance.process(),"Orsin: Nevím jaký postoj mám zaujmout.");
        assertEquals(cmdStance.process("sdfsdf","dsfdf"),"Orsin: Tomu nerozumím, neumím zaujmout oba postoje zároveň.");
        assertEquals(cmdStance.process("skryty"),"Zaujal jsi postoj 'skrytý'.");
        assertTrue(atributes.getStance());
        assertEquals(cmdStance.process("normalni"),"Zaujal jsi 'normální' postoj.");
        assertTrue(!atributes.getStance());
    }

}
