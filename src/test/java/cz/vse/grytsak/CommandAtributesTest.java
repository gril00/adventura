package cz.vse.grytsak;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import cz.vse.grytsak.Atributes;
import cz.vse.grytsak.CommandAtributes;
import cz.vse.grytsak.GamePlan;

import static org.junit.Assert.assertEquals;

/*******************************************************************************
 * Testovací třída {@code CommandAtributesTest} slouží ke komplexnímu otestování
 * třídy {@link CommandAtributesTest}.
 *
 * @author  Leon Grytsak
 * @version 1.0
 */
public class CommandAtributesTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }


    /***************************************************************************
     * Test of the {@link #setUp()} method preparing the test fixture.
     */
    @Test
    public void testCommandAtributes()
    {
        GamePlan plan = new GamePlan();
        CommandAtributes cmdAtributes = new CommandAtributes(plan);
        Atributes atributes = plan.getAtributes();
        assertEquals(cmdAtributes.process("dssfdfsd"),"K tomuto příkazu nezadávejte parametry.");
        String text = "Životy: " + atributes.getHealth() + "\nRychlost: " + atributes.getSpeed();
        assertEquals(cmdAtributes.process(),text);
        
    }

}
