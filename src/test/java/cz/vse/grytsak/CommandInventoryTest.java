package cz.vse.grytsak;


import cz.vse.grytsak.CommandInventory;
import cz.vse.grytsak.GamePlan;
import cz.vse.grytsak.Inventory;
import cz.vse.grytsak.Item;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída {@code CommandInventoryTest} slouží ke komplexnímu otestování
 * třídy {@link CommandInventoryTest}.
 *
 * @author  Leon Grytsak
 * @version 1.0
 */
public class CommandInventoryTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }



    /***************************************************************************
     * Metoda testuje příkaz CommandInventory
     */
    @Test
    public void testCommandInventory()
    {
        GamePlan plan = new GamePlan();
        CommandInventory cmdInv = new CommandInventory(plan);
        Inventory inventory = plan.getInventory();
        Item item = new Item("predmet", "ssdfsdfsd");
        
        assertEquals(cmdInv.process("dfsdf"),"K tomuto příkazu nezadávejte parametry.");        
        assertEquals(cmdInv.process(),"Orsin: Zatím jsem nic nesebral a nebo jsem předměty položil.");
        inventory.itemAdd(item);
        assertEquals(cmdInv.process(),"Orsin: V inventáři mám " + plan.getInventory().getItemsList() +".");
    }

}
