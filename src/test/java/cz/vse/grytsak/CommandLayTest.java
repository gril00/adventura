package cz.vse.grytsak;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import cz.vse.grytsak.*;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída {@code CommandLayTest} slouží ke komplexnímu otestování
 * třídy {@link CommandLayTest}.
 *
 * @author  Leon Grytsak
 * @version 1.0
 */
public class CommandLayTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }



    /***************************************************************************
     * Metoda testuje třídu CommandLay
     */
    @Test
    public void testCommandLay()
    {
        GamePlan plan = new GamePlan();
        CommandLay cmdLay = new CommandLay(plan);
        Inventory inventory = plan.getInventory();
        Area area =  new Area("lokace","sdfdfsdfsd");
        Item item = new Item("item","Ldfsdfsddfsdfůsd");
        inventory.itemAdd(item);
        plan.setCurrentArea(area);
        cmdLay.process("item");
        assertTrue(area.containsItem("item"));
    }

}
