package cz.vse.grytsak;

import cz.vse.grytsak.GamePlan;
import cz.vse.grytsak.Monster;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída {@code MonsterTest} slouží ke komplexnímu otestování
 * třídy {@link MonsterTest}.
 *
 * @author  Leon Grytsak
 * @version 1.0
 */
public class MonsterTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }

    /***************************************************************************
     * Testuje třídu Monster
     */
    @Test
    public void testMonsterAttack()
    {        
        GamePlan plan = new GamePlan();
        Monster monster = new Monster(plan);
        
        monster.monsterAttack();
        assertEquals(monster.getChance(), 15);
        plan.getKeyInventory().keyAdd();
        monster.monsterAttack();
        assertEquals(monster.getChance(), 23);
        plan.getKeyInventory().keyAdd();
        monster.monsterAttack();
        assertEquals(monster.getChance(), 30);
        plan.getKeyInventory().keyAdd();
        monster.monsterAttack();
        assertEquals(monster.getChance(), 38);
        plan.getKeyInventory().keyAdd();
        monster.monsterAttack();
        assertEquals(monster.getChance(), 46);
        plan.getKeyInventory().setRepairedKey();
        monster.monsterAttack();
        assertEquals(monster.getChance(), 46);
        
        monster.setChance(55);
        plan.getAtributes().setStance(true);
        monster.monsterAttack();
        double stanceDamage = monster.getStanceDamage();
        double damage2 = 1.5;
        assertTrue(stanceDamage < damage2);
        plan.getAtributes().setStance(false);
        assertTrue(!plan.getAtributes().getStance());
        
        plan.getAtributes().setShield(true);
        assertTrue(plan.getAtributes().getShield());
        monster.monsterAttack();
        assertTrue(plan.getAtributes().getShieldUsed());
        
        monster.monsterAttack();
        assertTrue(monster.getDamage() == 0);
    
    }

}
