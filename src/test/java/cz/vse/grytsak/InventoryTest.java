/* UTF-8 codepage: Příliš žluťoučký kůň úpěl ďábelské ódy. ÷ × ¤
 * «Stereotype», Section mark-§, Copyright-©, Alpha-α, Beta-β, Smile-☺
 */
package cz.vse.grytsak;


import cz.vse.grytsak.Inventory;
import cz.vse.grytsak.Item;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída {@code InventoryTest} slouží ke komplexnímu otestování
 * třídy {@link InventoryTest}.
 *
 * @author  Leon Grytsak
 * @version 1.0
 */
public class InventoryTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }


    //\TT== TESTS PROPER ===========================================================

    /***************************************************************************
     * Metoda testuje jestli jde do inventáře přidat víc než je maximální počet předmětů
     */
    @Test
    public void testAddOverMax()
    {
        Inventory inventory =  new Inventory();
        Item item1 = new Item("předmět1","dfdf");
        Item item2 = new Item("předmět2","dfdf554");
        Item item3 = new Item("předmět3","dfdfklů");
        Item item4 = new Item("předmět4","dfdfklů546l6544");
        
        Item item5 = inventory.itemAdd(item1);
        assertEquals(item5, item5);
        Item item6 = inventory.itemAdd(item1);
        assertEquals(item6, item6);
        Item item7 = inventory.itemAdd(item1);
        assertEquals(item7, item7);
       
        assertNull(inventory.itemAdd(item4));
    }
    
    /**
     * Metoda testuje zda se věc uspěšně vloží do inventáře
     */
    @Test
    public void testItemAdd()
    {
        Inventory inventory = new Inventory();
        Item item1 = new Item("předmět1","dfdf");
        assertEquals(item1, inventory.itemAdd(item1));
        assertEquals(true, inventory.containsItem("předmět1"));
    }
    
    /**
     * Metoda testuje zda se věc uspěšně smazala z inventáře
     */
    @Test
    public void testDeleteItem()
    {
        Inventory inventory = new Inventory();
        Item item1 = new Item("předmět1","dfdf");
        inventory.itemAdd(item1);
        assertEquals(item1, inventory.deleteItem("předmět1"));
        assertEquals("", inventory.getItemsList());
    }
}
