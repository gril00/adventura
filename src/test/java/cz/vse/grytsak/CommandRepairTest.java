package cz.vse.grytsak;


import cz.vse.grytsak.Area;
import cz.vse.grytsak.CommandRepair;
import cz.vse.grytsak.GamePlan;
import cz.vse.grytsak.KeyInventory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída {@code CommandRepairTest} slouží ke komplexnímu otestování
 * třídy {@link CommandRepairTest}.
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class CommandRepairTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }



    /***************************************************************************
     * Metoda testuje CommandRepair třídu
     */
    @Test
    public void testCommandRepair()
    {
        GamePlan plan = new GamePlan();
        CommandRepair cmdRepair = new CommandRepair(plan);
        KeyInventory invK = plan.getKeyInventory();
        Area staraKovarna = new Area("stara_kovarna","Stará kovárna. Tady bych možná mohl opravit klíč.");
        plan.setCurrentArea(staraKovarna);
        assertTrue(plan.getCurrentArea() == staraKovarna); 
        assertEquals(cmdRepair.process("dfsd"),"K tomuto příkazu nezadávejte parametry.");
        invK.keyAdd();
        invK.keyAdd();
        invK.keyAdd();
        assertEquals(cmdRepair.process(),"Orsin: Nemám dostatek částí na opravu klíče.");
        invK.keyAdd();
        assertEquals(cmdRepair.process(),"Orsin: Klíč byl opraven, teď mužů otevřít dveře ve staré budově a dostat se ke svému portálu.");
        assertTrue(invK.repairedKey());
    }

}
