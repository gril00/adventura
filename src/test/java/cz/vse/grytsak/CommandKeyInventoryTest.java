package cz.vse.grytsak;


import cz.vse.grytsak.CommandKeyInventory;
import cz.vse.grytsak.GamePlan;
import cz.vse.grytsak.KeyInventory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída {@code CommandKeyInventoryTest} slouží ke komplexnímu otestování
 * třídy {@link CommandKeyInventoryTest}.
 *
 * @author  Leon Grytsak
 * @version 1.0
 */
public class CommandKeyInventoryTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }


    /***************************************************************************
     * Metoda testuje KeyInventory
     */
    @Test
    public void testCommandKeyInventory()
    {
        GamePlan plan = new GamePlan();
        CommandKeyInventory cmdInvK = new CommandKeyInventory(plan);
        KeyInventory invK = plan.getKeyInventory();
        
        assertEquals(cmdInvK.process("ssfd"),"K tomuto příkazu nezadávejte parametry.");
        assertEquals(cmdInvK.process(),"V inventáří částí klíčů žádné části klíče nejsou.");
        invK.keyAdd();
        assertEquals(cmdInvK.process(),"V inventáři částí klíčů je: " + plan.getKeyInventory().keyParts() + " části klíčů.");
        invK.setRepairedKey();
        assertEquals(cmdInvK.process(),"V inventáři částí klíčů je opravený klíč");
        
    }

}
