package cz.vse.grytsak;

import org.junit.Before;
import org.junit.Test;
import cz.vse.grytsak.Game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Testovací třída pro komplexní otestování herního příběhu.
 *
 * @author Jarmila Pavlíčková
 * @author Jan Říha
 * @version LS 2020
 */
public class GameTest
{
    private Game game;

    @Before
    public void setUp()
    {
        game = new Game();
    }

    @Test
    public void testPlayerQuit()
    {
        assertEquals("vchod_do_lesa", game.getGamePlan().getCurrentArea().getName());

        /*game.processCommand("jdi les");
        assertEquals("les", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());

        game.processCommand("jdi temný_les");
        assertEquals("temný_les", game.getGamePlan().getCurrentArea().getName());
        assertFalse(game.isGameOver());

        game.processCommand("konec");
        assertTrue(game.isGameOver());*/
    }

}
