package cz.vse.grytsak;


import cz.vse.grytsak.Area;
import cz.vse.grytsak.CommandLocation;
import cz.vse.grytsak.GamePlan;
import cz.vse.grytsak.Item;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída {@code CommandLocationTest} slouží ke komplexnímu otestování
 * třídy {@link CommandLocationTest}.
 *
 * @author  Leon Grytsak
 * @version 1.0
 */
public class CommandLocationTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }



    /***************************************************************************
     * Test of the {@link #setUp()} method preparing the test fixture.
     */
    @Test
    public void testCommandLocation()
    {
        GamePlan plan = new GamePlan();
        CommandLocation cmdLocation = new CommandLocation(plan);
        Area area = new Area("lokace","dfsdfsd");
        Area baziny = new Area("baziny", "Jsi v bažinách. V dáli vidíš vchod do jeskyně a nebo předchod k řece.");
        Item boruvka = new Item("boruvky", "Mimozemská borůvka", true, false);
        area.addExit(baziny);
        area.addItem(boruvka);
        assertEquals(cmdLocation.process("dfsfsd"),"K tomuto příkazu nezadávejte parametry.");
        plan.setCurrentArea(area);
        assertEquals(cmdLocation.process(),area.getFullDescription());
    }

}
