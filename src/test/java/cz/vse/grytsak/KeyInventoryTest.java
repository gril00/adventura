package cz.vse.grytsak;

import cz.vse.grytsak.KeyInventory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída {@code KeyInventoryTest} slouží ke komplexnímu otestování
 * třídy {@link KeyInventoryTest}.
 *
 * @author  Leon Grytsak
 * @version 1.0
 */
public class KeyInventoryTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }

    /***************************************************************************
     * Testuje přidání klíče do inventáře klíčů
     */
    @Test
    public void testKeyAdd()
    {
        KeyInventory invK = new KeyInventory();
        invK.keyAdd();
        assertEquals(invK.keyParts(), 1);
    }
    
    /***************************************************************************
     * Testuje přidání opraveného klíče do inventáře klíčů.
     */
    @Test
    public void testSetRepairedKey()
    {
        KeyInventory invK = new KeyInventory();
        invK.setRepairedKey();
        assertEquals(invK.repairedKey(), true);
        assertEquals(invK.keyParts(), 0);
        
    }
}
